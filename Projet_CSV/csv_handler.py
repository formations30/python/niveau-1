import pandas
import os

def readCsv(path:str):  
    return pandas.read_csv(path, delimiter=';')
        

if __name__ == "__main__":
    path = os.path.basename("./personnes.csv")
    csvFile = readCsv(path)
    print(csvFile)
    print("Sarah Connor ?")
    sConnor = csvFile.loc[csvFile.prenom == 'Sarah']
    email = sConnor.email.iloc[0]
    if (email.__contains__("s.connor")):
        print("Yes ?")
    else:
        print("Next Door")