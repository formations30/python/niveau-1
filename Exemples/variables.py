#String
fruit_name = "Apple"
#Integer
number = 156
#Float
pi = 3.14
#List
persons = ["Sherlock", "Watson", "Doctor Who"]
#Dictionary
car = { "color": "blue", "brand":"BMW", "doors":5 }
#Boolean
response = True
